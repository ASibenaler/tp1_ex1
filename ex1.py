def fsum(a,b):
	return [a[0] + b[0],a[1] + b[1]]

def substract(a,b):
	return [a[0] - b[0],a[1] - b[1]]

def multiply(a,b):
	return [a[0] * b[0] - a[1] * b[1],a[0] * b[1] + a[1] * b[0]]
	
def divide(a,b):
	if((b[0]**2 + b[1]**2) != 0):
		return [(a[0] * b[0] + a[1] * b[1]) / (b[0]**2 + b[1]**2),(a[1] * b[0] + a[0] * b[1]) / (b[0]**2 + b[1]**2)]
	else:
		return "Division par 0 !"



if __name__ == '__main__':
	print("Premier nombre complexe : ")
	a1 = float(input('a1 '))
	a2 = float(input('a2 '))
	a = [a1,a2]	
	print("Second nombre complexe : ")
	b1 = float(input('b1 '))
	b2 = float(input('b2 '))
	b = [b1,b2]
	
	print("Somme : ", fsum(a,b))
	print("Soustraction : ", substract(a,b))
	print("Produit : ", multiply(a,b))
	print("Division : ", divide(a,b))
	
