## Exercice 1


# Objectif
L'objectif de cet exercice est de réaliser un script python permettant d'effectuer la somme, la différence, le produit et la division de deux nombres complexes, représentés sous forme d'un tuple de 2 réels.

# Réalisation
On réalise 4 fonctions réalisant les 4 opérations, ainsi qu'une fonction *main* qui demande à l'utilisateur d'entrer les parties réelles et imaginaires de chacun des complexes, et appel les différentes fonctions crées.
